#language:pt

Funcionalidade: Login
    Sendo um usuário do site QuintoAndar
    Posso realizar Login no site através do meu número de celular
    Para que somente eu possa visualizar minha conta

    Contexto: Página de login
        Dado que estou na página de Login do site QuintoAndar

    @smoke
    Cenário: Login primeiro acesso

        Quando preencho corretamente as informações do meu celular com "+<DDI>" "<DDD>" "<celular>"
        E confirmo a solicitação de login
        Então sou autenticado através do serviço de SMS
        E sou redirecionada para a tela de cadastro
        Quando informo meu "<nome>" e "<email>" válido
        Então realizo o login com sucesso
        E consigo visualizar minha página com minhas informações

    @smoke
    Cenário: Login usuário já cadastrado

        Quando preencho corretamente as informações do meu celular com "+<DDI>" "<DDD>" "<celular>"
        E confirmo a solicitação de login
        Então sou autenticada através do serviço de SMS
        E recebo um SMS com o código de acesso
        Quando informo corretamente o código de acesso
        Então realizo o login com sucesso
        E consigo visualizar minha página com minhas informações

    Cenário: Erro no envio do SMS

        Quando preencho as informaçoes do meu celular com dados válidos
        E confirmo a solicitação de login
        Então sou autenticada através do serviço de SMS
        E um SMS é enviado para o celular informado
        Quando não recebo o SMS
        E clico no link "Eu não recebi um código"
        Então sou redirecionado para a tela com as opções para resolução do erro

    Cenário: Login com dados inválidos

        Quando preencho incorretamente as informaçoes do meu celular
        Então os campos preenchidos incorretamente ficam destacados na cor vermelha
        E não sou autenticado no site
        E vejo a seguinte mensagem de erro "Insira um número de telefone válido"

    Cenário: Login sem preenchimento dos campos

        Quando não preencho algum dos campos para login
        Então os campos não preenchidos ficam destacados na cor vermelha
        E não sou autenticado no site
        E vejo a seguinte mensagem de erro "Insira um número de telefone válido"

    Cenário: Termos de uso e Política de Privacidade do login

        Quando clico no link "Termos de uso" ou "Política de Privacidade"
        Então sou redirecionado para a página em uma nova aba
        E automaticamente ancorado até o conteúdo acionado

    Cenário: Fechar página de login

        Quando clico no botão X para fechar a página de login
        Então sou redirecionado para a página em que estava anteriormente